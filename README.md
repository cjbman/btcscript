# Simple Bitcoin price tracker script in python
## Designed with status bars and widget panels in mind
## Customiseable with your choice of currency with an argument

To run, simply download the script then run "./btc2 GBP" (or your choice of currenc)"

e.g.

    ./btc GBP
would output

    £40470.86

Please make a pull request to add your currency!
